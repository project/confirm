
Confirm:
*****************

This module force user to preview posts and hide edit form 
in preview page.

Installation:
-------------
1. Install this files into the modules directory of your choice (for example, sites/all/modules/confirm).
2. Enable confirm.module in the admin/modules page

Settings:
---------
Use case 1: Using on all node posting form.
1. Check "Required and preview edit disabled" in the admin/content/node-settings page.

Use case 2: Setting per content type.
1. Check "Use confirm form for this content type" in the admin/content/node-type/[content type] page.


Author:
-------
Takashi Kabetani
kabetani@drupal.org
t-kabetani@texpress.co.jp
